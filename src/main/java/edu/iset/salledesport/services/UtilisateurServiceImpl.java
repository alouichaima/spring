package edu.iset.salledesport.services;

//import java.sql.Date;
//import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import edu.iset.salledesport.entities.Role;
import edu.iset.salledesport.entities.Utilisateur;
import edu.iset.salledesport.repositories.UtilisateurRepository;

@Service
public class UtilisateurServiceImpl implements UtilisateurService  {
    
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	// cryptage de password
	//@Autowired
	//private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	//get All
	@Override
	public List<Utilisateur> getAllUtlisateurs() {
		return utilisateurRepository.findAll();
	}
	
    //find Utilisateur by id
	@Override
	public Utilisateur findUtilisateurById(int id) {
        Optional<Utilisateur> utOptional=   utilisateurRepository.findById(id); 
        
		if (utOptional.isEmpty())
		{
			return null;
		}
		else 
		{
			return utOptional.get();
		}
	}
	
    //create utilisateur
	@Override
	public Utilisateur createUtilisateur(Utilisateur utilisateur) {
		//Cryptage de password de utilisateur
		/*String cryptedPassword = bCryptPasswordEncoder.encode(utilisateur.getPassword());
		utilisateur.setPassword(cryptedPassword);*/
		return utilisateurRepository.save(utilisateur);
	}
	
    //update utilisateur
	@Override
	public Utilisateur updateUtilisateur(Utilisateur utilisateur) {
        Optional<Utilisateur> utOptional=   utilisateurRepository.findById(utilisateur.getId()); 

        if (utOptional.isEmpty())
		{
			return null;
		}
		else 
		{
			return utilisateurRepository.save(utilisateur);
		}
	}
	
    //delete utilisateur
	@Override
	public void deleteUtilisateur(int id) {
		utilisateurRepository.deleteById(id);
	}
	
	// Find by nom
	@Override
	public List<Utilisateur> findByNom(String nom) {
		return utilisateurRepository.findByNom(nom);
	}
	
	//Find by nom and prenom
	@Override
	public List<Utilisateur> findByNomAndPrenom(String nom, String prenom) {
		return utilisateurRepository.findByNomAndPrenomWithJPQLWithNamedParametrs(nom, prenom); 
	}
	
	
	
	//Find by date
	/*@Override
	public List<Utilisateur> findByDatenaiss(Date datenaiss) {
		return utilisateurRepository.findByDate(datenaiss);
	}*/
	
	//méthode de colloboration de notre user avec user de spring security
	/*
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        Utilisateur utilisateur = utilisateurRepository.findByEmail(email);
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for(Role r : utilisateur.getRole()) {
			GrantedAuthority authority= new SimpleGrantedAuthority(r.getTitre());
			authorities.add(authority);
		}
		
		return new User(utilisateur.getEmail(),utilisateur.getPassword(),authorities);
	}*/
	

	// Find by Role
	/*@Override
	public List<Utilisateur> findByTitre(String titre) {
		return utilisateurRepository.findByTitre(titre);
	}*/

	//Find by Email
	/*@Override
	public Utilisateur findByEmail(String email) {
		Utilisateur utilisateur = utilisateurRepository.findByEmail(email); 
		return null;
	}*/
	
	
	
	
	
	
	
}
