package edu.iset.salledesport.services;

import java.sql.Date;
import java.util.List;

//import org.springframework.security.core.userdetails.UserDetailsService;

import edu.iset.salledesport.entities.Utilisateur;

public interface UtilisateurService /*extends UserDetailsService */{
	
	//methodes CRUD Bassiques 
	public List<Utilisateur> getAllUtlisateurs();
	public Utilisateur findUtilisateurById(int id);
	public Utilisateur createUtilisateur(Utilisateur utilisateur);
	public Utilisateur updateUtilisateur(Utilisateur utilisateur);
	public void deleteUtilisateur(int id);
	
	//methodes avancees 
	
	public List<Utilisateur> findByNom(String nom);
	public List<Utilisateur> findByNomAndPrenom(String nom , String prenom);
	/*public List<Utilisateur> findByDatenaiss(Date datenaiss);*/
	/*public List<Utilisateur> findByTitre(String titre);*/
	




}
