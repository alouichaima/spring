package edu.iset.salledesport.entities;

import java.io.Serializable;
//import java.util.List;

import javax.persistence.Column;
//import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.OneToMany;
import javax.persistence.Table;

import edu.iset.salledesport.repositories.RoleRepository;





@Entity
@Table(name = "roles")
public class Role implements Serializable {
	@Id

	private Long id; 
	
	@Enumerated(EnumType.STRING)
	  @Column(length = 20)
	private ERole name;
	
	public Role() {

	  }
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public ERole getName() {
		return name;
	}
	public void setName(ERole name) {
		this.name = name;
	}


	

	
	

}
