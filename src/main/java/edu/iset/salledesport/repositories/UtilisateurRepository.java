package edu.iset.salledesport.repositories;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import edu.iset.salledesport.entities.Utilisateur;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer> {
	

	public List<Utilisateur> findByNom(String nom);
	public Utilisateur findByEmail(String email);
	public Utilisateur findByPrenom(String prenom);


	public List<Utilisateur> findByNomAndPrenom(String nom , String prenom);
	
	@Query("SELECT u FROM Utilisateur u WHERE u.nom= ?1 AND u.prenom= ?2")
	public List<Utilisateur> findByNomAndPrenomWithJPQL(String nom , String prenom);

	@Query("SELECT u FROM Utilisateur u WHERE u.nom LIKE :myNom OR u.prenom LIKE :myPrenom")
	public List<Utilisateur> findByNomAndPrenomWithJPQLWithNamedParametrs(@Param(value = "myNom")   String nom ,@Param(value = "myPrenom") String prenom);
	
	/*public List<Utilisateur> findByDatenaiss(Date datenaiss);*/
	/*public List<Utilisateur> findByTitre(String titre);*/
	public boolean existsByprenom(String prenom);
	public boolean existsByEmail(String email);
	
	
}
