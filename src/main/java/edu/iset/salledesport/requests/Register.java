package edu.iset.salledesport.requests;

import java.sql.Date;
import java.util.Set;

public class Register {
	private String nom;
	private String prenom;
	private Date datenaiss;
	private String email;
	private String password;
	private int telephone;
	private String typesport;
	private double poids;
	
	private Set<String> role;

	

	@Override
	public String toString() {
		return "Register [nom=" + nom + ", prenom=" + prenom + ", datenaiss=" + datenaiss + ", email=" + email
				+ ", password=" + password + ", telephone=" + telephone + ", typesport=" + typesport + ", poids="
				+ poids + ", role=" + role + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDatenaiss() {
		return datenaiss;
	}

	public void setDatenaiss(Date datenaiss) {
		this.datenaiss = datenaiss;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

	public String getTypesport() {
		return typesport;
	}

	public void setTypesport(String typesport) {
		this.typesport = typesport;
	}

	public double getPoids() {
		return poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	public Set<String> getRole() {
		return role;
	}

	public void setRole(Set<String> role) {
		this.role = role;
	}
}
