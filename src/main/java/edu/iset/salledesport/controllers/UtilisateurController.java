package edu.iset.salledesport.controllers;

import java.sql.Date;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.iset.salledesport.entities.Utilisateur;
import edu.iset.salledesport.requests.NomAndPrenomRequest;
import edu.iset.salledesport.services.UtilisateurService;

@RestController
@CrossOrigin(origins = "http://localhost:50740")
@RequestMapping ("/utilisateur")//localhost:8080/utilisateur
public class UtilisateurController {
	
	@Autowired
	private UtilisateurService utilisateurService;
	
	//getall
	@GetMapping /*(path ="/getAllUtilisateurs")*///localhost:8080/utilisateur/getAllUtilisateurs
	public List<Utilisateur> getAllUtilisateurs()
	
	{
		return utilisateurService.getAllUtlisateurs();
	}
	
	//get par id 
	@GetMapping(path="/{id}") //localhost:8080/utilisateur/3
	public ResponseEntity<Utilisateur>  findUtilisateurById(@PathVariable  int id)
	
	{
		Utilisateur utilisateur= utilisateurService.findUtilisateurById(id);
		if (utilisateur==null)
		{
			return new ResponseEntity<Utilisateur>(HttpStatus.NO_CONTENT);
		}
		else 
		{
			return new ResponseEntity<Utilisateur>(utilisateur,HttpStatus.OK);
		}
		
	}
	
	//get par nom
	@GetMapping(path="/findByNom/{nom}") //localhost:8080/utilisateur/findByNom/wafa
	public ResponseEntity<List<Utilisateur>>  findUtilisateurByNom(@PathVariable  String nom)
	
	{
		List<Utilisateur> utilisateurs= utilisateurService.findByNom(nom);
		if (utilisateurs.isEmpty())
		{
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		}
		else 
		{
			return new ResponseEntity<List<Utilisateur>>(utilisateurs,HttpStatus.OK);
		}
		
	}
	
	//get par nom et prenom
	
	@GetMapping(path="/findByNomAndPrenom/{nom}/{prenom}") //localhost:8080/utilisateur/findByNomAndPrenom/wafa/neji
	public ResponseEntity<List<Utilisateur>>  findUtilisateurByNomAndPrenom(@PathVariable  String nom, @PathVariable String prenom)
	
	{
		List<Utilisateur> utilisateurs= utilisateurService.findByNomAndPrenom(nom , prenom);
		if (utilisateurs.isEmpty())
		{
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		}
		else 
		{
			return new ResponseEntity<List<Utilisateur>>(utilisateurs,HttpStatus.OK);
		}
		
	}
	
	@GetMapping(path="/findByNomAndPrenomWithRB") //localhost:8080/utilisateur/findByNomAndPrenom/wafa/neji
	public ResponseEntity<List<Utilisateur>>  findUtilisateurByNomAndPrenom(@RequestBody  NomAndPrenomRequest nomAndprenomRequest)
	
	{
		List<Utilisateur> utilisateurs= utilisateurService.findByNomAndPrenom(nomAndprenomRequest.getNom() , nomAndprenomRequest.getPrenom());
		if (utilisateurs.isEmpty())
		{
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		}
		else 
		{
			return new ResponseEntity<List<Utilisateur>>(utilisateurs,HttpStatus.OK);
		}
		
	}
	
	
	/*@GetMapping(path="/findByDate/{datenaiss}") //localhost:8080/utilisateur/findByDate/1999-04-06
	public ResponseEntity<List<Utilisateur>>  findUtilisateurByDate(@PathVariable Date datenaiss)
	
	{
		List<Utilisateur> utilisateurs= utilisateurService.findByDate(datenaiss);
		if (utilisateurs.isEmpty())
		{
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		}
		else 
		{
			return new ResponseEntity<List<Utilisateur>>(utilisateurs,HttpStatus.OK);
		}
		
	}*/
	
	
	/*@GetMapping(path="/findByIdWithRequestParam") //localhost:8080/utilisateur/findByIdWithRequestParam?id=2
	public ResponseEntity<Utilisateur>  findUtilisateurById(@RequestParam int id)
	
	{
		Utilisateur utilisateur= utilisateurService.findUtilisateurById(id);
		if (utilisateur==null)
		{
			return new ResponseEntity<Utilisateur>(HttpStatus.NO_CONTENT);
		}
		else 
		{
			return new ResponseEntity<Utilisateur>(utilisateur,HttpStatus.OK);
		}
		
	}*/
	
	
	/*@GetMapping(path = "/findByTitre/{titre}") // localhost:8080/utilisateur/findByTitre/ADMIN
	public ResponseEntity<List<Utilisateur>> findByTitre(@PathVariable String titre) {
		List<Utilisateur> utilisateurs = utilisateurService.findByTitre(titre);

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}*/
	
	@PostMapping
	public Utilisateur creationUtilisateur(@RequestBody  Utilisateur utilisateur)
	{
		return utilisateurService.createUtilisateur(utilisateur);
	}
	
	@PutMapping
	public Utilisateur updateUtilisateur(@RequestBody  Utilisateur utilisateur)
	{
		return utilisateurService.updateUtilisateur(utilisateur);
	}
	
	@DeleteMapping(path="/{id}") //localhost:8080/utilisateur/3
	public void deleteUtilisateur(@PathVariable int id)
	{
       utilisateurService.deleteUtilisateur(id);	
       
	}
	
	
	

}
