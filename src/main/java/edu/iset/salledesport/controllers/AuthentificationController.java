package edu.iset.salledesport.controllers;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.iset.salledesport.entities.ERole;
import edu.iset.salledesport.entities.Role;
import edu.iset.salledesport.entities.Utilisateur;
import edu.iset.salledesport.repositories.RoleRepository;
import edu.iset.salledesport.repositories.UtilisateurRepository;
import edu.iset.salledesport.requests.Register;
import response.ResponseMessage;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/auth")
public class AuthentificationController {

	@Autowired
	private UtilisateurRepository utilisateurRepository;

	@Autowired
	private RoleRepository roleRepository;

	/*@Autowired
	PasswordEncoder encoder;*/

	@PostMapping("register")
	public ResponseEntity<ResponseMessage> registerUser(@Valid @RequestBody Register registerRequest) {
		if (utilisateurRepository.existsByprenom(registerRequest.getPrenom())) {
			return new ResponseEntity<>(new ResponseMessage("prenom is already taken !"), HttpStatus.BAD_REQUEST);
		}

		if (utilisateurRepository.existsByEmail(registerRequest.getEmail())) {
			return new ResponseEntity<>(new ResponseMessage("Email is already taken !"), HttpStatus.BAD_REQUEST);
		}

		// Create user account
		Utilisateur user = new Utilisateur (registerRequest.getNom(),
				                             registerRequest.getPrenom(), 
				                             registerRequest.getDatenaiss(),
				                            registerRequest.getEmail(),
				                            registerRequest.getPassword(),
				                            registerRequest.getTelephone(),
				                            registerRequest.getTypesport(),
				                            registerRequest.getPoids());
		Set<String> rolesInRequest = registerRequest.getRole();
		Set<Role> roles = new HashSet<>();
		if (rolesInRequest == null) {
		      Role abonneRole = roleRepository.findByName(ERole.ROLE_ABONNE)
		          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		      roles.add(abonneRole );
		    } else {
		rolesInRequest.forEach(role -> {
			switch (role) {
			case "admin":
				Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
				          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(adminRole);
				break;
			 case "entraineur":
				Role entraineurRole = roleRepository.findByName(ERole.ROLE_ENTRAINEUR)
				          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(entraineurRole);
				break;
			 default:
				 Role abonneRole = roleRepository.findByName(ERole.ROLE_ABONNE)
				          .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(abonneRole);

			}

		});
		    }
		user.setRoles(roles);
		;
		utilisateurRepository.save(user);

		return new ResponseEntity<>(new ResponseMessage("user Registred succesfully "), HttpStatus.OK);
	}

}
